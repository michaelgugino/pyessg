import pyessg
import os


class TestPyEssg(object):

    def test_basic_site(self):
        examples_dir = os.environ['PYESSG_EXAMPLES_BASIC']
        ifp = os.path.join(examples_dir, 'site.yml')
        opd = '/tmp/pyessg_output'
        p_gen = pyessg.Pyessg(input_file_path=ifp, output_dir=opd)
        assert p_gen.status == 0

    def test_cli(self):
        # out = check_call(['apt-deps', 'apt'])
        assert 0 == 0
