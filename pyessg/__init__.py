"""Main parsing class"""
import os
import yaml
from jinja2 import Template


def read_yaml(input_file_path):
    """Read in yml file at input_file_path"""
    with open(input_file_path) as ifile:
        data = ifile.read()

    datad = yaml.load(data)
    return datad


class Pyessg(object):
    """pyessg generating class"""

    def __init__(self, input_file_path, output_dir, file_ext='.html.yml'):
        """Initialize pyessg class"""
        self.input_file_path = input_file_path
        self.input_file_dir = os.path.dirname(self.input_file_path)
        self.output_dir = output_dir
        # self.overwrite_output = overwrite_output
        self.file_ext = file_ext
        self.site_dict = read_yaml(self.input_file_path)
        self.dirs_to_gen = self.site_dict['directories_to_generate']
        self.checked_dirs = set()
        self.run()
        self.status = 0

    def find_directory_files(self, dir_def):
        """Parse directory definition"""
        directory_config = self.site_dict[dir_def]
        directory_path = directory_config['directory']
        files_to_parse = []
        if not directory_path.startswith('/'):
            directory_path = os.path.join(self.input_file_dir, directory_path)

        for dirpath, dirnames, filenames in os.walk(directory_path):
                for filename in filenames:
                    if filename.endswith(self.file_ext):
                        files_to_parse.append(os.path.join(dirpath, filename))
        return files_to_parse

    def read_template_part(self, page_part_path):
        """Read jinja2/html from disk"""
        with open(page_part_path) as ifile:
            data = ifile.read()
        return data

    def create_jinja_template(self, dir_def):
        """Create jinja template from specified page parts"""
        page_parts = self.site_dict[dir_def]['page_parts']

        html_templates = []
        for part in page_parts:
            if not part.startswith('/'):
                part = os.path.join(self.input_file_dir, part)
            html_templates.append(self.read_template_part(part))
        return Template("\n".join(html_templates))

    def make_output_dir(self, dir_to_make):
        os.makedirs(dir_to_make, exist_ok=True)
        self.checked_dirs.add(dir_to_make)

    def generate_html(self, dir_def, data, jtemplate, output_path):
        dir_to_make = output_path.split('/')
        dir_to_make = '/'.join(dir_to_make[0:len(dir_to_make) - 1])
        if dir_to_make not in self.checked_dirs:
            self.make_output_dir(dir_to_make)
        print(dir_to_make)
        with open(output_path, 'w') as ofile:
            ofile.write(jtemplate.render(data))

    def generate_output_path(self, dir_def, item):
        directory_config = self.site_dict[dir_def]
        path_to_trim = directory_config['directory']
        output_relative_destination = directory_config['destination']
        if output_relative_destination == '/':
            output_relative_destination = ''
        if not path_to_trim.startswith('/'):
            path_to_trim = os.path.join(self.input_file_dir, path_to_trim)
        # split paths into list to easily bisect filepath.
        path_to_trim = path_to_trim.split('/')
        item = item.split('/')
        opath = item[len(path_to_trim):]
        # Join with the path specified by the user.
        opath = os.path.join(self.output_dir, output_relative_destination,
                             '/'.join(opath))

        # Trim the last file extension in them.
        # (ie, trim .yml from filename.html.yml)
        return os.path.splitext(opath)[0]

    def create_output_files(self, dir_def, files_to_parse, jtemplate):
        """Parse the files and write them to disk"""
        extra_vars = self.site_dict[dir_def].get('extra_vars') or {}
        for item in files_to_parse:
            file_data = read_yaml(item)
            # copy extra_vars so it's not spoiled for other pages
            data = extra_vars.copy()
            data.update(file_data)
            # We need to trim the file name to determine correct output path
            output_path = self.generate_output_path(dir_def, item)
            # This actually generates and writes the data to disk.
            self.generate_html(dir_def, data, jtemplate, output_path)

    def run(self):
        """Execute creation of site"""
        for dir_def in self.dirs_to_gen:
            print("parsing directory {}".format(str(dir_def)))
            # Find all the files in the specified directory
            files_to_parse = self.find_directory_files(dir_def)
            print(files_to_parse)
            # Combine the page_parts into a jinja template.
            jtemplate = self.create_jinja_template(dir_def)
            # Read in the data from the pages, combine with template,
            # and write the files.
            self.create_output_files(dir_def, files_to_parse, jtemplate)
