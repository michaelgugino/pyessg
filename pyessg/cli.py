"""Command line script for pyessg"""
import argparse
import pyessg


def main():
    """This executes the pyessg cli script"""
    parser = argparse.ArgumentParser(
        description='Generate Static HTML sites with HTML, YAML, and Jinja2')
    parser.add_argument('site_yaml', metavar='SITE_YAML',
                        help='yaml file that defines the site.')
    parser.add_argument('output_dir', metavar='OUTPUT_DIR',
                        help='Output directory')
    args = parser.parse_args()

    ifp = args.site_yaml
    opd = args.output_dir
    p_gen = pyessg.Pyessg(input_file_path=ifp, output_dir=opd)
    return p_gen.status
