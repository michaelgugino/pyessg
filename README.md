# Python Easy Static Site Generator (Machine)

Now that HTML doesn't suck, static sites are cool again.

Static Site Generators still suck because they are:
1. Primarily not written in Python
2. The Python ones are complicated and don't fit my needs
3. Where's the YAML?

# Install and usage
## Install:
```sh
pip install pyessg
```

## Usage
```sh
pyessg site.yml /path/to/output
```

Example:
```sh
pyessg examples/basic/site.yml example_output/
```

## Usage Details
Usage details, aka, 'the deets'

You must provide a yaml file (aka, site.yml) that specifies the following:
```yaml
directories_to_generate:
  - tld

tld:
  directory: tld
  destination: '/'
  page_parts:
    - header.html
    - page_template.html
  extra_vars: {}
```

'directories_to_generate' specifies the names of the dictionaries inside of
that file that are going to be generated.

For each in directories_to_generate, the following three variables are
mandatory:
1. directory - the absolute or relative path of directory containing pages to
generate.  Relative path is relative to site.yml.
2. destination - the directory path relative to the output path specified via
the command line invocation.
3. page_parts - Jinja2 template parts (or frankly, can be just plain html) that
will be joined together in the order specified.  pyessg will read each file
from disk and join them as strings, so do not use template extensions and
template blocks.  Paths are relative to the site.yml

Finally, one optional variable, 'extra_vars' provides default values that will
be merged and overwritten by any variables in each page.

See examples/basic/site.yml for more information.

## Where's the YAML?

YAML is the best.  It's used by ansible and is easily parsed by Python.

This project uses the YAML as well.  Write your YAML, write your HTML,
generate your files.

## Other Frameworks...
Come with an embedded webserver?  Something about NPM?  None of that here.

This framework takes YAML + HTML = WEBSITE.

No having to write RST/MD files just to convert to HTML...(what's the point of
this exercise?)

## Why Trust Python Easy Static Site Generator (Machine) (aka pyessg)?

1. Not written in Java - No shady binaries or tons of compilation required here.
2. Nothing to do with nodejs.  I'm not a front-end developer, the last thing
I want to write is javascript to run my applications.
3. Linux first: I wrote this software to be utilized from within a venv on Linux,
which is the OS I use. (I say Linux instead of GNU/Linux, but GNU is a very
important part of the modern operation system)
4. No magic here.  No 50-classes-deep object-oriented obfuscation.  Just clean,
easy to follow code.

## Why is the acronym 'pyessg' when the name of the project includes '(Machine)'
The name does not include '(Machine)', I don't know what you're talking about.
